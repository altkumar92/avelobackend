<?php

use Illuminate\Support\Facades\Route;

/*
 |------------------------------------------------
 |   User SignUp and Login Route(s)
 |------------------------------------------------
 */
Route::post('/home/user/check', 'API\Auth\LoginController@userExists');
Route::post('/home/user/login', 'API\Auth\LoginController@userLogin');
Route::post('/home/user/initiate', 'API\Auth\RegisterController@userInitiateRegistration');
Route::post('/home/user/register', 'API\Auth\RegisterController@userCompleteRegistration');

/*
 |------------------------------------------------
 |   Driver SignUp and Login Route(s)
 |------------------------------------------------
 */
Route::post('/home/driver/check', 'API\Auth\LoginController@driverExists');
Route::post('/home/driver/login', 'API\Auth\LoginController@driverLogin');
Route::post('/home/driver/initiate', 'API\Auth\RegisterController@driverInitiateRegistration');
Route::post('/home/driver/register', 'API\Auth\RegisterController@driverCompleteRegistration');
Route::post('/home/driver/documents/upload', 'App\UploadsController@uploadDriverDocuments');

/*
 |------------------------------------------------
 |   Display Image Upload and Download Routes
 |------------------------------------------------
 */
Route::post('/home/avatar/upload', 'App\UploadsController@uploadAvatar');
Route::get('/home/avatar/download', 'App\UploadsController@downloadAvatar');


/*
|------------------------------------------------
 |   Driver Routes
|------------------------------------------------
 */

Route::post('update/driver/location','App\DriverController@updateLocation');
Route::post('all/driver/list','App\CustomerController@GetAllDriver');
Route::post('home/customer/profile/token/update', 'App\FirebaseController@updateToken');
Route::post('home/customer/hireNow', 'App\CustomerController@HireNow');
Route::post('home/customer/conformHire', 'App\HireNowConformController@HireNowConfirm2');
Route::post('home/driver/booking', 'App\BookingController@GetDriverBookingData');
Route::post('home/user/booking', 'App\BookingController@GetUserBookingData');


Route::post('home/driver/accept/booking', 'App\DriverController@AcceptBooking');
Route::post('home/driver/reject/booking', 'App\DriverController@DriverRejectBooking');

Route::post('home/driver/arrived/trip', 'App\DriverController@driver_arrived_trip');
Route::post('home/driver/start/trip', 'App\DriverController@driver_on_trip');
Route::post('home/driver/complete/trip', 'App\DriverController@driver_completed_trip');

Route::post('home/user/reject/trip', 'App\CustomerController@user_reject_trip');





