<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookingdetails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('user_id');
            $table->string('purpose')->nullable();
            $table->string('pickup_area');
            $table->string('drop_area');
            $table->string('pickup_date_time')->nullable();
            $table->string('bike_id')->nullable();
            $table->string('departure_date_time')->nullable();
            $table->integer('status')->nullable();
            $table->string('status_code')->nullable();
            $table->string('promo_code')->nullable();
            $table->string('book_create_date_time')->nullable();
            $table->String('distance')->nullable();
            $table->integer('amount')->nullable();
            $table->string('approx_time')->nullable();
            $table->integer('final_amount')->nullable();
            $table->string('pickup_address')->nullable();;
            $table->integer('assigned_for')->nullable();
            $table->string('payment_type')->nullable();
            $table->string('transaction_id')->nullable();
            $table->integer('km')->nullable();;
            $table->string('timetype')->nullable();;
            $table->string('comment')->nullable();
            $table->string('driver_status')->nullable();
            $table->string('pickup_lat');
            $table->string('pickup_long');
            $table->string('drop_lat');
            $table->string('drop_long');
            $table->string('flag')->nullable();
            $table->string('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookingdetails');
    }
}
