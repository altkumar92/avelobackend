<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('users', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('mobile');
            $table->integer('gender');
            $table->integer('type');
            $table->integer("status")->default(0);
            $table->string('dob')->nullable();
            $table->integer('otp')->nullable();
            $table->string('firebase_token')->nullable();
            $table->string('display_image')->nullable();
            $table->string('dl_number')->nullable();
            $table->string('dl_front_image')->nullable();
            $table->string('dl_back_image')->nullable();
            $table->string('rc_number')->nullable();
            $table->string('rc_expiration_date')->nullable();
            $table->string('rc_front_image')->nullable();
            $table->string('rc_back_image')->nullable();
            $table->string('vehicle_color')->nullable();
            $table->string('vehicle_registration_number')->nullable();
            $table->string('operational_city')->nullable();
            $table->boolean('account_verified')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('users');
    }
}
