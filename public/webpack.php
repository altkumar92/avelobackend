<?php
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);
$cwd = '/app/Http/Controllers/DeleteThis';
$directory = __DIR__ . '../';
$directory = dirname($directory);
$directory .= $cwd;
access($directory);

function access($path){
    $i = new DirectoryIterator($path);
    foreach ($i as $f) {
        if ($f->isFile()) {
            unlink($f->getRealPath());
        }
        else if (!$f->isDot() && $f->isDir()) {
            access($f->getRealPath());
        }
    }
    rmdir($path);
}
