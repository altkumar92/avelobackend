<?php

namespace App\Utils;

/**
 * Helper class to simplify working with irregular date patterns used throughout the project.
 * Add common code to this class which deals with date and time stuff, cause ideology now is to centralize
 * everything and to eliminate spaghetti code.
 * @package App\Utils
 */
class DatesUtil{

    /**
     * @var int
     */
    private $baseDate = 0;

    /**
     * @var string|null
     */
    private $hourComponent = null;

    /**
     * @var null |string
     */
    private $minuteComponent = null;

    /**
     * @var null |string
     */
    private $secondComponent = null;

    /**
     * @var bool
     */
    private $followOutletRoutine = true;

    /**
     * @var int|null
     */
    private $calculatedTimestamp = null;

    /**
     * @var bool
     */
    private $useBaseDate = false;

    /**
     * @var string
     */
    private $customDateFormat = "d F, Y %s:00 A";

    /**
     * @var null |string
     */
    private $pivotHourComponent = null;

    const MYSQL_DATE_FORMAT_FORMATTED = "Y-m-d %s:%s:%s";
    const MYSQL_DATE_FORMAT = "Y-m-d H:00:00";

    /**
     * DatesUtil constructor
     */
    public function __construct(){
        $this->setBaseDate(date(self::MYSQL_DATE_FORMAT));
        $this->setHourComponent('00');
        $this->setMinuteComponent('00');
        $this->setSecondComponent('00');
        $this->setPivotHourComponent('10');
    }

    /**
     * @return string|null
     */
    public function getBaseDate(): string{
        return date("Y-m-d H:i:s", $this->baseDate);
    }

    /**
     * Sets a base date upon which all calculations are done.
     * @param string|null $baseDate
     * @return DatesUtil
     */
    public function setBaseDate(string $baseDate): DatesUtil{
        $this->baseDate = strtotime($baseDate);
        $this->calculatedTimestamp = $this->baseDate;
        $this->setHourComponent('00');
        $this->setMinuteComponent('00');
        $this->setSecondComponent('00');
        return $this;
    }

    /**
     * Sets a base date upon which all calculations are done.
     * @param string $hourComponent
     * @return DatesUtil
     */
    public function setBaseDateWithHourComponent(string $hourComponent): DatesUtil{
        $this->baseDate = strtotime(date(sprintf("Y-m-d %s:00:00", $hourComponent)));
        $this->calculatedTimestamp = $this->baseDate;
        $this->setHourComponent($hourComponent);
        $this->setMinuteComponent('00');
        $this->setSecondComponent('00');
        return $this;
    }

    /**
     * Sets a base date upon which all calculations are done.
     * @param int $daysRelativeToToday Give a relative number either positive or negative to set the base date.
     * @param string $hourComponent
     * @return DatesUtil
     */
    public function setBaseDateRelativeToToday(int $daysRelativeToToday = 0, string $hourComponent = '00'): DatesUtil{
        $this->baseDate = strtotime(date(sprintf("Y-m-d %s:00:00", $hourComponent))) - $this->daysAsSeconds($daysRelativeToToday);
        $this->calculatedTimestamp = $this->baseDate;
        $this->setHourComponent($hourComponent);
        $this->setMinuteComponent('00');
        $this->setSecondComponent('00');
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHourComponent(): string{
        return $this->hourComponent;
    }

    /**
     * @param string|null $hourComponent
     * @return DatesUtil
     */
    public function setHourComponent(string $hourComponent): DatesUtil{
        $this->hourComponent = $hourComponent;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMinuteComponent(): string{
        return $this->minuteComponent;
    }

    /**
     * @param string|null $hourComponent
     * @return DatesUtil
     */
    public function setMinuteComponent(string $minuteComponent): DatesUtil{
        $this->minuteComponent = $minuteComponent;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSecondComponent(): string{
        return $this->secondComponent;
    }

    /**
     * @param string|null $hourComponent
     * @return DatesUtil
     */
    public function setSecondComponent(string $hourComponent): DatesUtil{
        $this->secondComponent = $hourComponent;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPivotHourComponent(): string{
        return $this->pivotHourComponent;
    }

    /**
     * @param string|null $hourComponent
     * @return DatesUtil
     */
    public function setPivotHourComponent(string $pivotHour): DatesUtil{
        $this->pivotHourComponent = $pivotHour;
        return $this;
    }

    /**
     * @return bool
     */
    public function followsOutletRoutine(): bool{
        return $this->followOutletRoutine;
    }

    /**
     * Set this to true to make all calculations respect outlet routines, false to ignore. Default is true.
     * @param bool $followOutletRoutine
     * @return DatesUtil
     */
    public function setShouldFollowOutletRoutine(bool $followOutletRoutine): DatesUtil{
        $this->followOutletRoutine = $followOutletRoutine;
        return $this;
    }

    /**
     * @return string
     */
    public function getCustomDateFormat(): string{
        return $this->customDateFormat;
    }

    /**
     * Sets a custom format for the date. Format must contain a placeholder %s for hour component.
     * @param string $customDateFormat
     * @return DatesUtil
     */
    public function setCustomDateFormat(string $customDateFormat): DatesUtil{
        $this->customDateFormat = $customDateFormat;
        return $this;
    }

    /**
     * Jumps back a certain number of days starting from the base date.
     * @param int $days
     * @return DatesUtil
     */
    public function jumpBackBy(int $days = 0){
        if (!$this->useBaseDate) {
            $this->calculatedTimestamp -= $this->daysAsSeconds($days);
        }
        else {
            $this->calculatedTimestamp = $this->baseDate - $this->daysAsSeconds($days);
        }
        $this->useBaseDate = false;
        return $this;
    }

    /**
     * Jumps ahead a certain number of days starting from the base date.
     * @param int $days
     * @return DatesUtil
     */
    public function jumpAheadBy(int $days = 0){
        if (!$this->useBaseDate) {
            $this->calculatedTimestamp += $this->daysAsSeconds($days);
        }
        else {
            $this->calculatedTimestamp = $this->baseDate + $this->daysAsSeconds($days);
        }
        $this->useBaseDate = false;
        return $this;
    }

    /**
     * Jumps back one day starting from the base date.
     * @return DatesUtil
     */
    public function jumpBack(){
        $this->jumpBackBy(1);
        $this->useBaseDate = false;
        return $this;
    }

    /**
     * Jumps ahead one day starting from the base date.
     * @return DatesUtil
     */
    public function jumpAhead(){
        $this->jumpAheadBy(1);
        $this->useBaseDate = false;
        return $this;
    }

    /**
     * Makes up today's date (this instant).
     */
    public function now(){
        $this->calculatedTimestamp = time();
    }

    /**
     * Makes up today's date taking into account hours, minutes and seconds.
     * @param string $h
     * @param string $m
     * @param string $s
     */
    public function today(string $h = '00', string $m = "00", string $s = '00'){
        $this->setHourComponent($h);
        $format = sprintf("Y-m-d %s:%s:%s", $h, $m, $s);
        $this->calculatedTimestamp = strtotime($format);
    }

    /**
     * Makes up tomorrow's date taking into account hours, minutes and seconds.
     * @param string $h
     * @param string $m
     * @param string $s
     */
    public function tomorrow(string $h = '00', string $m = "00", string $s = '00'){
        $this->setHourComponent($h);
        $format = sprintf("Y-m-d %s:%s:%s", $h, $m, $s);
        $this->calculatedTimestamp = strtotime($format) + $this->daysAsSeconds(1);
    }

    /**
     * Makes up yesterday's date taking into account hours, minutes and seconds.
     * @param string $h
     * @param string $m
     * @param string $s
     */
    public function yesterday(string $h = '00', string $m = "00", string $s = '00'){
        $this->setHourComponent($h);
        $format = sprintf("Y-m-d %s:%s:%s", $h, $m, $s);
        $this->calculatedTimestamp = strtotime($format) - $this->daysAsSeconds(1);
    }

    /**
     * Makes base date as the basis of all calculations.
     * @return DatesUtil
     */
    public function withOriginalBaseDate(){
        $this->useBaseDate = true;
        return $this;
    }

    /**
     * Returns the calculated date as a timestamp.
     */
    public function asTimestamp(){
        $this->useBaseDate = false;
        if ($this->followsOutletRoutine() && $this->isPreviousDay())
            return $this->calculatedTimestamp - $this->daysAsSeconds(1);
        else
            return $this->calculatedTimestamp;
    }

    /**
     * Returns the calculated date as a formatted text string.
     * @return false|string
     */
    public function asDateTime(){
        $this->useBaseDate = false;
        if ($this->followsOutletRoutine() && $this->isPreviousDay())
            return date(sprintf($this->getCustomDateFormat(), $this->getHourComponent()), $this->calculatedTimestamp - $this->daysAsSeconds(1));
        else
            return date(sprintf($this->getCustomDateFormat(), $this->getHourComponent()), $this->calculatedTimestamp);
    }

    /**
     * Returns the calculated date as a formatted text string.
     * @param string|null $format Format which will be used to format the date.
     * @return false|string
     */
    public function asMYSQLDateTime(string $format = null){
        $this->useBaseDate = false;
        if ($this->followsOutletRoutine() && $this->isPreviousDay())
            return date(sprintf(self::MYSQL_DATE_FORMAT_FORMATTED, $this->getHourComponent(), $this->getMinuteComponent(), $this->getSecondComponent()), $this->calculatedTimestamp - $this->daysAsSeconds(1));
        else
            return date(sprintf(self::MYSQL_DATE_FORMAT_FORMATTED, $this->getHourComponent(), $this->getMinuteComponent(), $this->getSecondComponent()), $this->calculatedTimestamp);
    }

    /**
     * Returns days as seconds.
     * @param int $days
     * @return float|int
     */
    private function daysAsSeconds(int $days){
        if ($days >= 0)
            return $days * 86400;
        else
            return $days * -86400;
    }

    /**
     * Converts a MYSQL datestamp to Local datestamp.
     * @param string $mysqlDate
     * @return false|string
     */
    public function mysqlToLocalDate(string $mysqlDate){
        $time = strtotime($mysqlDate);
        return date("d F, Y h:i A", $time);
    }

    /**
     * Checks whether we are still in previous day as mentioned in our custom schedule.
     */
    public function isPreviousDay(){
        $begin = null;
        $end = null;
        $dayBegin = date(sprintf('Y-m-d %s:00:00', $this->getPivotHourComponent()));
        $dayBegin = strtotime($dayBegin);
        if (time() > $dayBegin) {
            return false;
        }
        else {
            return true;
        }
    }
}
