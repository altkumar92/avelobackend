<?php

namespace App\Utils\Firebase;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Provides methods to build a notification and transmit it.
 * @package App\Utils\Firebase
 */
class Notification{

	/**
	 * Firebase Api key.
	 */
	const FirebaseApiKey = "key=AAAAXwtDNS4:APA91bEZDiFPXTf_gmJGATqqqFVpjeU_8_WYdFTlVeqKv5xSUCf--UUsvjlJxNwIvsZA34TKaFAJkAwZPA1PVldYjlTSQgIZ-Kpi0atx9ITP6q2fxrpjzwj9SuNKaOf202FWo3n6n5GL";

	/**
	 * Firebase GCM endpoint.
	 */
	const FirebaseCloudURL = 'https://fcm.googleapis.com/fcm/send';

	/**
	 * Application icon to display in notification.
	 */
	const AppIcon = "appicon";

	/**
	 * @var string
	 */
	private $token = null;

	/**
	 * @var string
	 */
	private $title = null;

	/**
	 * @var string
	 */
	private $message = null;

	/**
	 * @var Client
	 */
	private $client = null;

	/**
	 * Notification constructor.
	 */

	private  $userData=array();


	public function __construct(){
		$this->client = new Client();
	}

	/**
	 * @return string
	 */
	public function getToken(): string{
		return $this->token;
	}

	/**
	 * @param string $token
	 * @return Notification
	 */
	public function setToken(string $token): Notification{
		$this->token = $token;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string{
		return $this->title;
	}

	/**
	 * @param string $title
	 * @return Notification
	 */
	public function setTitle(string $title): Notification{
		$this->title = $title;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getMessage(): string{
		return $this->message;
	}


	/**
	 * @param string $message
	 * @return Notification
	 */
	public function setMessage(string $message): Notification{
		$this->message = $message;
		return $this;
	}

    public function setUserData($userData){

        $this->userData = $userData;
        return $this;
    }

    public function getUserData():array{
        return $this->userData;
    }

	private function requestHeaders(){
		return [
			'Authorization' => self::FirebaseApiKey,
			'Content-Type' => 'application/json',
			'Accept' => 'application/json'
		];
	}
	private function dataBody(){
		return [
			'body' => $this->getMessage(),
			'title' => $this->getTitle(),
			'icon' => self::AppIcon,
            'userData'=>$this->getUserData()
		];
	}

    private function notificationBody(){
        return [
            'body' => $this->getMessage(),
            'title' => $this->getTitle(),
            'icon' => self::AppIcon,
            'userData'=>$this->getUserData()
        ];
    }

	/**
	 * Sends the notification payload to Firebase server to be delivered to target device(s).
	 */
	public function transmit(){
		try {
			$response = $this->client->request('POST', self::FirebaseCloudURL, [
				'json' => ['to' => $this->getToken(), 'data' => $this->dataBody()],
				'headers' => $this->requestHeaders(),
			]);
			$jsonResponse = json_decode($response->getBody(), true);
			return $jsonResponse['success'];
		}
		catch (GuzzleException $e) {
			return false;
		}
	}

    public function transmitNotification(){
        try {
            $response = $this->client->request('POST', self::FirebaseCloudURL, [
                'json' => ['to' => $this->getToken(), 'notification' => $this->notificationBody()],
                'headers' => $this->requestHeaders(),
            ]);
            $jsonResponse = json_decode($response->getBody(), true);
            return $jsonResponse['success'];
        }
        catch (GuzzleException $e) {
            return false;
        }
    }

}