<?php

namespace App\Utils;

use Illuminate\Support\Facades\Storage;

/**
 * Provides features to upload and download files.
 * @package App\Utils
 */
class UploadsUtil{

    /**
     * Directories listing for typographical error prevention.
     */
    const Directories = [
        'avatars',
        'docs'
    ];

    /**
     * Avatars directory recognition constant.
     */
    const Avatars = 0;

    /**
     * Documents directory recognition constant.
     */
    const Documents = 1;

    /**
     * Uploads a file to specified directory.
     * @param mixed $image Path of image from client (Request->file)
     * @param string $directory Name of directory to create.
     * @return string Relative path of image file
     */
    public function uploadMultiImage($image, string $directory){
        return Storage::putFile($directory, $image, 'public');
    }

    /**
     * Uploads a display image for an user.
     * @param mixed $filename
     * @return string|null
     */
    public function uploadAvatar($filename){
        try {
            $path = $this->uploadMultiImage($filename, self::Directories[self::Avatars]);
            return $path;
        }
        catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Uploads multiple images and returns relatives path names for all images.
     * @param array $documents
     * @return array|null
     */
    public function uploadDriverDocuments(array $documents){
        $rcFront = $documents['rcFront'];
        $rcBack = $documents['rcBack'];
        $dlFront = $documents['dlFront'];
        $dlBack = $documents['dlBack'];
        $paths = [
            'rcFront' => null,
            'rcBack' => null,
            'dlFront' => null,
            'dlBack' => null,
        ];
        try {
            $paths['rcFront'] = $this->uploadMultiImage($rcFront, self::Directories[self::Documents]);
            $paths['rcBack'] = $this->uploadMultiImage($rcBack, self::Directories[self::Documents]);
            $paths['dlFront'] = $this->uploadMultiImage($dlFront, self::Directories[self::Documents]);
            $paths['dlBack'] = $this->uploadMultiImage($dlBack, self::Directories[self::Documents]);
            return $paths;
        }
        catch (\Exception $e) {
            return null;
        }
    }
}
