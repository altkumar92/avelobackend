<?php
/**
 * Created by PhpStorm.
 * User: Alok Kumar
 * Date: 9/8/2019
 * Time: 2:44 AM
 */

namespace App\Utils;


class DistanceCalculator
{

    const DistanceURL = 'https://maps.googleapis.com/maps/api/distancematrix/json?origins=';
    const apiKey='AIzaSyBsKXUHqa5DcNk9_usVfjjFSPgwxqTiai4';

    public function GetDrivingDistance($lat1, $lat2, $long1, $long2)
    {
        $url = self::DistanceURL.$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=en&key=".self::apiKey;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result, true);
        $distance = $response['rows'][0]['elements'][0]['distance']['text'];
        $time = $response['rows'][0]['elements'][0]['duration']['text'];

       return array('distance' => $distance, 'time' => $time);
    }

}