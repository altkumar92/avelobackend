<?php

namespace App\Http\Controllers\App;

use App\Models\FirebaseToken;
use App\Utils\Firebase\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FirebaseController extends Controller
{
    /**
     * Method names reference, defined here to prevent typographical errors.
     * @var array
     */
    const MethodNames = [
        'UpdateToken' => 'updateToken'
    ];

    /**
     * FirebaseController constructor.
     */
    public function __construct(){

    }

    public function sendNotification(Request $request){
        $customerId = $request->customerId;
        $title = $request->title;
        $message = $request->message;
        try {
            $token = FirebaseToken::where('customer_id', $customerId)->firstOrFail();
            $notification = new Notification();
            $result = $notification->setTitle($title)->setMessage($message)->setToken($token->token)->transmit();
            return $result;
        }
        catch (ModelNotFoundException $exception) {
            return false;
        }
    }

    public function updateToken(Request $request){
        $model = new FirebaseToken();
        return $model->updateToken($request);
    }
}
