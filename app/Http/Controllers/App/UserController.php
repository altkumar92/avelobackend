<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\ApiController;
use App\Models\OtpMapping;
use App\Models\PersonalAccessClient;
use App\Models\User;
use App\Models\UserSession;
use App\Utils\GUID;
use App\Utils\OtpUtils;
use App\Utils\ValidationsUtil;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Class UserController
 * @package App\Http\Controllers\API
 */
class UserController extends ApiController{

    /**
     * Method names reference, defined here to prevent typographical errors.
     * @var array
     */
    const MethodNames = [
        'NewSession' => 'makeNewSession',
        'UpdateMobile' => 'updateMobile',
        'UpdateToken' => 'updateToken'
    ];

    /**
     * Makes a new session for a user.
     * @param Request $r
     * @return array
     */
    public function makeNewSession(Request $r){
        $guid = GUID::make();
        $userSession = new UserSession();
        $userSession->session_id = $guid;
        $userSession->save();
        return ['status' => 1, 'message' => "Session initialized successfully.", 'sessionId' => $guid];
    }

    /**
     * Returns user model if a user is found with that mobile number, false otherwise.
     * @param string $mobile
     * @return bool|User
     */
    public function findUserByMobile(string $mobile){
        return User::where('phone', $mobile)->first();
    }

    /**
     * Begins registration procedure for a user.
     * Takes a mobile number and verifies it before
     * attempting to register.
     * @param Request $r
     * @return array
     */
    public function initiateRegistration(Request $r){

    }

    /**
     * Completes registration of a user.
     * Takes various details such as name, phone, and otp
     * and register him.
     * @param Request $r
     * @return array
     */
    public function completeRegistration(Request $r){

    }

    /**
     * Updates mobile number of a customer/user.
     * @param Request $request
     * @return JsonResponse
     */
    public function updateMobile(Request $request){
        /*Initiate mobile number update request.*/
        if ($request->isMethod('GET')) {
            $validationsUtil = new ValidationsUtil();
            $validationsUtil->setFields(['newNumber']);
            if (!$validationsUtil->hasAllFields($request->all())) {
                return response()->json([
                    'status' => 0,
                    'message' => $validationsUtil->getValidationErrorString()
                ]);
            }

            $validator = Validator::make($request->all(), [
                'newNumber' => ['bail', 'digits:10'],
            ], [
                'newNumber.digits' => "Mobile number must be of 10 digits.",
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'status' => 0,
                    'message' => $validator->errors()->first()
                ]);
            }

            $newMobile = $request->newNumber;
            $user = User::where('phone', $newMobile)->first();
            if ($user != null) {
                return response()->json([
                    'status' => 0,
                    'message' => "The number you entered is already taken. Please enter a new one."
                ]);
            }
            else {
                try {
                    $otp = OtpUtils::generate();
                    OtpUtils::sendMobileUpdateOtp($otp, $newMobile);
                    try {
                        $otpMapping = OtpMapping::where('mobile', $newMobile)->firstOrFail();
                        $otpMapping->otp = $otp;
                        $otpMapping->save();
                        return response()->json([
                            'status' => 1,
                            'message' => "We have sent an OTP at this number. Please verify it to continue."
                        ]);
                    }
                    catch (ModelNotFoundException $e) {
                        $otpMapping = new OtpMapping();
                        $otpMapping->mobile = $newMobile;
                        $otpMapping->otp = $otp;
                        $otpMapping->save();
                        return response()->json([
                            'status' => 1,
                            'message' => "We have sent an OTP at this number. Please verify it to continue."
                        ]);
                    }
                }
                catch (\Exception $e) {
                    return response()->json([
                        'status' => 0,
                        'message' => "We could not send an OTP at this mobile number. Please try again."
                    ]);
                }
            }
        }

        /*Complete mobile number update request after verifying OTP sent.*/
        if ($request->isMethod('POST')) {
            $validationsUtil = new ValidationsUtil();
            $validationsUtil->setFields(['customerId', 'newNumber', 'otp']);
            if (!$validationsUtil->hasAllFields($request->all())) {
                return response()->json([
                    'status' => 0,
                    'message' => $validationsUtil->getValidationErrorString()
                ]);
            }

            $validator = Validator::make($request->all(), [
                'customerId' => ['bail', 'numeric', 'exists:customers,id'],
                'newNumber' => ['bail', 'digits:10'],
            ], [
                'customerId.numeric' => "Customer Id must be numeric.",
                'customerId.digits' => "We could not find a customer with that Id.",
                'newNumber.digits' => "Mobile number must be of 10 digits.",
            ]);
            if ($validator->fails()) {
                return response()->json([
                    'status' => 0,
                    'message' => $validator->errors()->first()
                ]);
            }

            $customerId = $request->customerId;
            $newMobile = $request->newNumber;
            $otp = $request->otp;
            try {
                $otpMapping = OtpMapping::where('mobile', $newMobile)->firstOrFail();
                if ($otpMapping->otp != $otp) {
                    return response()->json([
                        'status' => 0,
                        'message' => "Your entered OTP does not match with the one we sent to your mobile."
                    ]);
                }

                $customer = null;
                try {
                    $user = User::findOrFail($customerId);
                    $user->phone = $newMobile;
                    $user->save();
                    return response()->json([
                        'status' => 1,
                        'message' => "Your mobile number has been updated successfully . "
                    ]);
                }
                catch (\Exception $e) {
                    return response()->json([
                        'status' => 0,
                        'message' => "We could not update your mobile number."
                    ]);
                }
            }
            catch (ModelNotFoundException $e) {
                return response()->json([
                    'status' => 0,
                    'message' => "We did not find any OTP generated for your mobile number. Please try again."
                ]);
            }
        }
        return response()->json([
            'status' => 0,
            'message' => "Invalid request method."
        ]);
    }
}
