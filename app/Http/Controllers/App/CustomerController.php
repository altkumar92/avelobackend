<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\ApiController;
use App\Models\bookingdetails;
use App\Utils\DistanceCalculator;
use ErrorException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Utils\ValidationsUtil;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\driver_location;

class CustomerController extends ApiController
{

    /**
     * Maximum allowed distance.
     * In Kilometres.
     */
    const MaxAllowedDistance = 10;


    public function GetAllDriver(Request $r)
    {
        $validatorUtils=new ValidationsUtil();
        $validatorUtils->setFields(['latitude','longitude']);
        if (!$validatorUtils->hasAllFields($r->all()))
        {
            return [
                'status' => 0,
                'message'=> $validatorUtils->getValidationErrorString()
            ];
        }
        $validator = Validator::make($r->all(), [
            'latitude' => ['bail', 'required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'longitude' => ['bail', 'required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
        ], [
            'latitude.required' => 'Latitude is required',
            'longitude.required' => 'Latitude is required',
            'latitude.regex' => 'Latitude is of incorrect format.',
            'longitude.regex' => 'Longitude is of incorrect format.'
        ]);
        if ($validator->fails())
        {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }
        $latitude = $r->latitude;
        $longitude = $r->longitude;
        $results = [];
        $conditions = [
            ['driver_lat', '!=', 0],
            ['driver_long', '!=', 0]
        ];
        $Coordinates = driver_location::where($conditions)->get();
        foreach ($Coordinates as $Coordinate) {
            $Latitude = $Coordinate->driver_lat;
            $Longitude = $Coordinate->driver_long;
            $options = [
                'sourceLat' => $latitude,
                'sourceLong' => $longitude,
                'destinationLat' => $Latitude,
                'destinationLong' => $Longitude
            ];
            $distance = $this->distanceBetweenCoordinates($options);
            //dd($distance);
            if ($distance <=self::MaxAllowedDistance){

                $results[] = [
                    'latitude' => $Coordinate->driver_lat,
                    'longitude' =>$Coordinate->driver_long
                ];

            }
        }
        if (count($results) > 0) {

            return response()->json([
                'status' => 1,
                'message' => 'list of driver',
                'data' => $results
            ]);
        }
        else {
            return response()->json([
                'status' => 0,
                'message' => 'We are sorry, but there are no driver to your location.'
            ]);
        }

    }



    public function HireNow(Request $r)
    {
        $validatorUtils = new ValidationsUtil();
        $validatorUtils->setFields(['pickup_lat', 'pickup_long', 'drop_lat', 'drop_long']);
        if (!$validatorUtils->hasAllFields($r->all())) {
            return [
                'status' => 0,
                'message' => $validatorUtils->getValidationErrorString()
            ];
        }
        $validator = Validator::make($r->all(), [
            'pickup_lat' => ['bail', 'required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'pickup_long' => ['bail', 'required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'drop_lat' => ['bail', 'required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'drop_long' => ['bail', 'required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
        ], [
            'pickup_lat.required' => 'Latitude is required',
            'pickup_long.required' => 'Longitude is required',
            'pickup_lat.regex' => 'Latitude is of incorrect format.',
            'pickup_long.regex' => 'Longitude is of incorrect format.',

            'drop_lat.required' => 'Latitude is required',
            'drop_long.required' => 'Longitude is required',
            'drop_lat.regex' => 'Latitude is of incorrect format.',
            'drop_long.regex' => 'Longitude is of incorrect format.'

        ]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }

        $options = [
            'sourceLat' => $r->pickup_lat,
            'sourceLong' => $r->pickup_long,
            'destinationLat' => $r->drop_lat,
            'destinationLong' => $r->drop_long
        ];

        try {
            $distanceCalculator = new DistanceCalculator();
            $distanceArr = $distanceCalculator->GetDrivingDistance($options['sourceLat'], $options['destinationLat'], $options['sourceLong'], $options['destinationLong']);
            $distance = preg_replace('/[^\\d.]+/', '', $distanceArr['distance']);
            $time = preg_replace('/[^\\d.]+/', '', $distanceArr['time']);
            $fare = $this->fareCalculate($distance, $time);
            $data = [
                'amount' => $fare,
                'distance' => $distanceArr['distance'],
                'time' => $distanceArr['time']
            ];
            return response()->json([
                'status' => 1,
                'message' => 'total amount is',
                'data' => $data
            ]);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => 0,
                'message' => 'please try again',
            ]);
        }

    }

    protected function fareCalculate($distance, $tripTime)
    {

        $fare=15; //upto 3 km
        try
        {
            if($distance>3) // greater than 3 KM - RS3/KM
            {
                //----rs 1 for insurance
                $fare+=1;
                // trip charge RS1/Minute
                $fare+=(1*$tripTime);
                if($distance<=5)// less than 5 KM
                {
                    $fare+=(3*($distance-3));
                }
                else
                {
                    $fare+=6;
                }
            }
            if($distance>5)// greater than 5 KM - RS4.5/KM
            {
                if($distance<=8)// less than 8 KM
                {
                    $fare+=(4.5*($distance-5));
                }
                else
                {
                    $fare+=(4.5*3);
                }
            }
            if($distance>8)// greater than 8 KM - RS5/KM
            {
                if($distance<=10)// less than 10 KM
                {
                    $fare+=(5*($distance-8));
                }
                else{
                    $fare+=(5*2);
                }
            }
            if($distance>10)// greater than 10 KM - RS6/KM
            {
                $fare+=(6*($distance-10));
            }
        }
        catch(\Exception $e)
        {
            abort(500, 'Internal Server Error');
        }
        return round($fare);
    }



    public function distanceBetweenCoordinates(array $options){
        $sourceLat = $options['sourceLat'];
        $sourceLong = $options['sourceLong'];
        $destinationLat = $options['destinationLat'];
        $destinationLong = $options['destinationLong'];
        $theta = $sourceLong - $destinationLong;
        $dist = sin(deg2rad($sourceLat)) * sin(deg2rad($destinationLat)) + cos(deg2rad($sourceLat)) * cos(deg2rad($destinationLat)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $distance = $dist * 60 * 1.1515 * 1.609344;
        return intval($distance);
    }


   public function user_reject_trip(Request $request)
   {
       $validatorUtils=new ValidationsUtil();
       $validatorUtils->setFields(['bookingId','driverId']);
       if (!$validatorUtils->hasAllFields($$request->all()))
       {
           return [
               'status' => 0,
               'message'=> $validatorUtils->getValidationErrorString()
           ];
       }
       $validator=Validator::make($request->all(),[

           'bookingId'=>['required'],
           'driverId'=>['required']

       ]);
       if ($validator->fails())
       {
           return [
               'status' => 0,
               'message' => $validator->errors()->first(),
               'fields' => $validator->errors()
           ];
       }
       $bookingData=new bookingdetails();
       $resultData=$bookingData->user_reject_trip($request);
       if ($resultData)
       {
           if($resultData->status==4) {
               $bookingData->updateimmidiatedriverstatus($request->bookingId);
           }
           return response()->json([
               'status' => 1,
               'message' => 'Your booking details ',
               'data' =>$resultData
           ]);

       }else{

           return response()->json([
               'status'=>0,
               'message'=>''
           ]);
       }


   }







}
