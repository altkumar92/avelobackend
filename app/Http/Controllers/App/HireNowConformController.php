<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\ApiController;
use App\Models\bookingdetails;
use App\Models\Driver_availability;
use App\Models\driver_location;
use App\Models\driver_status;
use App\Models\FirebaseToken;
use App\Models\User;
use App\Utils\Firebase\Notification;
use App\Utils\ValidationsUtil;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class HireNowConformController extends ApiController
{

    const MaxAllowedDistanceForNearestOutlet = 10;


    public function ConformHire(Request $request)
    {

        $validatorUtils=new ValidationsUtil();
        $validatorUtils->setFields(['user_id', 'username', 'pickup_area', 'drop_area', 'distance', 'approx_time',
            'amount', 'final_amount', 'payment_type','pickup_lat','pickup_long','drop_lat','drop_long']);
        if (!$validatorUtils->hasAllFields($request->all()))
        {
            return [
                'status' => 0,
                'message'=> $validatorUtils->getValidationErrorString()
            ];
        }
        $validator=Validator::make($request->all(),[

            'user_id'=>['required'],
            'username'=>['required'],
            'pickup_area'=>['required'],
            'drop_area'=>['required'],
            'distance'=>['required'],
            'approx_time'=>['required'],
            'amount'=>['required'],
            'final_amount'=>['required'],
            'payment_type'=>['required'],
            'pickup_lat'=>['required'],
            'pickup_long'=>['required'],
            'drop_lat'=>['required'],
            'drop_long'=>['required']
        ]);
        if ($validator->fails())
        {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }
        $results = [];
        $conditions = [
            ['driver_lat', '!=', 0],
            ['driver_long', '!=', 0]
        ];
        $Coordinates = driver_location::where($conditions)->get();
        foreach ($Coordinates as $Coordinate) {
            $Latitude = $Coordinate->driver_lat;
            $Longitude = $Coordinate->driver_long;
            $options = [
                'sourceLat' => $request->pickup_lat,
                'sourceLong' => $request->pickup_long,
                'destinationLat' => $Latitude,
                'destinationLong' => $Longitude
            ];

            $distance = $this->distanceBetweenCoordinates($options);
            if ($distance <=self::MaxAllowedDistanceForNearestOutlet){

                $results[] = [
                    'driver_id'=>$Coordinate->driver_id,
                    'distance'=>$distance,
                    'latitude' => $Coordinate->driver_lat,
                    'longitude' =>$Coordinate->driver_long
                ];

            }
        }
        if (count($results) > 0)
        {
            $data=collect($results);
            $min = $data->where('distance', $data->min('distance'))->first();
            $bookingDetails=new bookingdetails();
            $results=$bookingDetails->CreateBookingId($request);
            if ($results)
            {
                $title = "Incomming Booking";
                $message = "You have received new Booking";
                try {
                    $msg = array
                    (
                        "bookingId" => $results->id,
                        "userId"=>$results->user_id
                    );
                    $token = FirebaseToken::where('customer_id', $min['driver_id'])->firstOrFail();
                    $notification = new Notification();
                    $result = $notification->setTitle($title)->setMessage($message)->setUserData($msg)->setToken($token->token)->transmit();
                    if ($result)
                    {
                        return response()->json([
                            'status' => 1,
                            'bookingId'=>$results->id,
                            'message' =>'Wait for confirmation'
                        ]);
                    }else{
                        return response()->json([
                            'status' => 0,
                            'message' => 'SomeThing went wrong'
                        ]);
                    }
                }
                catch (ModelNotFoundException $exception) {
                    return response()->json([
                        'status' => 0,
                        'message' => 'SomeThing went wrong'
                    ]);

                }


            }else{

                return response()->json([
                'status' => 0,
                'message' => 'unsuccess for creating booking id'
            ]);
            }
        }
        else {
            return response()->json([
                'status' => 0,
                'message' => 'We are sorry, but there are no driver to your location.',
            ]);
        }







    }


    public function  HireNowConfirm2(Request $request)
    {
        $validatorUtils=new ValidationsUtil();
        $validatorUtils->setFields(['user_id', 'username', 'pickup_area', 'drop_area', 'distance', 'approx_time',
            'amount', 'final_amount', 'payment_type','pickup_lat','pickup_long','drop_lat','drop_long']);
        if (!$validatorUtils->hasAllFields($request->all()))
        {
            return [
                'status' => 0,
                'message'=> $validatorUtils->getValidationErrorString()
            ];
        }
        $validator=Validator::make($request->all(),[

            'user_id'=>['required'],
            'username'=>['required'],
            'pickup_area'=>['required'],
            'drop_area'=>['required'],
            'distance'=>['required'],
            'approx_time'=>['required'],
            'amount'=>['required'],
            'final_amount'=>['required'],
            'payment_type'=>['required'],
            'pickup_lat'=>['required'],
            'pickup_long'=>['required'],
            'drop_lat'=>['required'],
            'drop_long'=>['required']
        ]);
        if ($validator->fails())
        {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }
        $results = [];
        $conditions = [
            ['driver_lat', '!=', 0],
            ['driver_long', '!=', 0]
        ];
        $Coordinates = driver_location::where($conditions)->get();
        foreach ($Coordinates as $Coordinate) {
            $Latitude = $Coordinate->driver_lat;
            $Longitude = $Coordinate->driver_long;
            $options = [
                'sourceLat' => $request->pickup_lat,
                'sourceLong' => $request->pickup_long,
                'destinationLat' => $Latitude,
                'destinationLong' => $Longitude
            ];

            $distance = $this->distanceBetweenCoordinates($options);
            $checkStatus=User::where('id','=',$Coordinate->driver_id)->first();
            if ($distance <=self::MaxAllowedDistanceForNearestOutlet && $checkStatus->status==1){

                $results[] = [
                    'driver_id'=>$Coordinate->driver_id,
                    'distance'=>$distance,
                    'latitude' => $Coordinate->driver_lat,
                    'longitude' =>$Coordinate->driver_long
                ];

            }
        }
        if (count($results) > 0)
        {

           //check driver status is available or not
            $resultData = [];
            foreach ($results as $result)
            {
                $availability=Driver_availability::where('driverId','=',$result['driver_id'])->first();
                 if ($availability)
                 {
                     $availData=Driver_availability::where('driverId','=',$result['driver_id'])
                     ->where(function ($query){
                         $query->where('driver_flag','=',2)
                               ->orWhere('driver_flag','=',3);
                      })->first();
                     if ($availData){

                         $resultData[] = [
                             'driver_id'=>$result['driver_id'],
                             'distance'=>$result['distance'],
                             'latitude' => $result['latitude'],
                             'longitude' =>$result['latitude']
                         ];
                     }

                 }else{

                     $resultData[] = [
                         'driver_id'=>$result['driver_id'],
                         'distance'=>$result['distance'],
                         'latitude' => $result['latitude'],
                         'longitude' =>$result['latitude']
                     ];
                 }
            }

            if (count($resultData)>0)
            {
                $sorted = collect($resultData)->sortBy('distance');
                $min = $sorted->where('distance', $sorted->min('distance'))->first();
                $bookingDetails=new bookingdetails();
                $createBId=$bookingDetails->CreateBookingId($request);
                if ($createBId)
                {
                    $DavailData=Driver_availability::where('driverId','=',$min['driver_id'])->first();
                    if ($DavailData){
                        Driver_availability::where('driverId','=',$min['driver_id'])->update([
                            'driver_flag'=>0
                        ]);
                    }else{
                        $driverData=new Driver_availability();
                        $driverData->driverId=$min['driver_id'];
                        $driverData->driver_flag=0;
                        $driverData->save();
                    }
                    $data=new driver_status();
                    $data->driver_id=$min['driver_id'];
                    $data->booking_id=$createBId->id;
                    $data->driver_flag=0;
                    $data->save();

                    $title = "Incomming Booking";
                    $message = "You have received new Booking";
                    try {
                        $msg = array
                        (
                            "bookingId" => $createBId->id,
                            "userId"=>$createBId->user_id
                        );
                        $token = FirebaseToken::where('customer_id', $min['driver_id'])->firstOrFail();
                        $notification = new Notification();
                        $resultN = $notification->setTitle($title)->setMessage($message)->setUserData($msg)->setToken($token->token)->transmit();
                        if ($resultN)
                        {
                            return response()->json([
                                'status' => 1,
                                'bookingId'=>$createBId->id,
                                'message' =>'Wait for confirmation'
                            ]);
                        }else{
                            return response()->json([
                                'status' => 0,
                                'message' => 'SomeThing went wrong'
                            ]);
                        }
                    }
                    catch (ModelNotFoundException $exception) {
                        return response()->json([
                            'status' => 0,
                            'message' => 'Model Not Found Exception'
                        ]);

                    }


                }else{

                    return response()->json([
                        'status' => 0,
                        'message' => 'Error in creating booking id'
                    ]);
                }

            }else
            {
                return response()->json([
                    'status' => 0,
                    'message' => 'Oops! all our drivers are busy handling other client’s. We regret for inconvenience caused.',
                ]);
            }
        }
        else {
            return response()->json([
                'status' => 0,
                'message' => 'We are sorry, but there are no driver to your location.',
            ]);
        }







    }


    public function distanceBetweenCoordinates(array $options){
        $sourceLat = $options['sourceLat'];
        $sourceLong = $options['sourceLong'];
        $destinationLat = $options['destinationLat'];
        $destinationLong = $options['destinationLong'];
        $theta = $sourceLong - $destinationLong;
        $dist = sin(deg2rad($sourceLat)) * sin(deg2rad($destinationLat)) + cos(deg2rad($sourceLat)) * cos(deg2rad($destinationLat)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $distance = $dist * 60 * 1.1515 * 1.609344;
        return intval($distance);
    }


}
