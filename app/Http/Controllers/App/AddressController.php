<?php

namespace App\Http\Controllers\API;

use App\Models\Address;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

/**
 * Class AddressController
 * @package App\Http\Controllers\API
 */
class AddressController extends ApiController{

	/**
	 * Method names reference, defined here to prevent typographical errors.
	 * @var array
	 */
	const MethodNames = [
		'Add' => 'addAddress',
		'Update' => 'updateAddress',
		'Delete' => 'deleteAddress',
		'GetAll' => 'readAllAddresses'
	];

	/**
	 * Saves an address for a customer.
	 * @param Request $r
	 * @return array
	 */
	public function add(Request $r){
		$version = $r->version;
		$model = new Address();
		return $model->callMethodVersion(self::MethodNames['Add'], $r, !isset($version) ? 1 : $version);
	}

	/**
	 * Updates an existing address for a customer.
	 * @param Request $r
	 * @return array
	 */
	public function update(Request $r){
		$version = $r->version;
		$model = new Address();
		return $model->callMethodVersion(self::MethodNames['Update'], $r, !isset($version) ? 1 : $version);
	}

	/**
	 * Gets all addresses stored for a customer.
	 * @param Request $r
	 * @return array
	 */
	public function readAll(Request $r){
		$version = $r->version;
		$model = new Address();
		return $model->callMethodVersion(self::MethodNames['GetAll'], $r, !isset($version) ? 1 : $version);
	}

	/**
	 * Deletes an address for a user.
	 * @param Request $r
	 * @return array
	 */
	public function delete(Request $r){
		$version = $r->version;
		$model = new Address();
		return $model->callMethodVersion(self::MethodNames['Delete'], $r, !isset($version) ? 1 : $version);
	}
}
