<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\ApiController;
use App\Models\bookingdetails;
use App\Models\Driver_availability;
use App\Models\driver_location;
use App\Models\driver_status;
use App\Models\FirebaseToken;
use App\Utils\Firebase\Notification;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Utils\ValidationsUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class DriverController extends ApiController
{

    const MaxAllowedDistanceForNearestOutlet = 10;

    public function updateLocation(Request $r)
    {
         $validatorUtils=new ValidationsUtil();
         $validatorUtils->setFields(['driverId','driver_lat','driver_long']);
         if (!$validatorUtils->hasAllFields($r->all()))
         {
             return [
               'status' => 0,
               'message'=> $validatorUtils->getValidationErrorString()
             ];
         }
         $validator=Validator::make($r->all(),[

             'driverId'=>['required'],
             'driver_lat'=>['required'],
             'driver_long'=>['required']
         ]);
         if ($validator->fails())
         {
            return [
                 'status' => 0,
                 'message' => $validator->errors()->first(),
                 'fields' => $validator->errors()
             ];
         }

         $driver=null;
         $driver=driver_location::where('driver_id',$r->driverId)->first();
         if ($driver)
         {
             $update=driver_location::where('driver_id',$r->driverId)->update([
                 'driver_lat'=>$r->driver_lat,
                 'driver_long'=>$r->driver_long
             ]);

             if ($update){
                 return response()->json([
                     'status'=>1,
                     'message'=>"Successfully update location"
                 ]);
             }
             else{
                 return response()->json([
                     'status'=>0,
                     'message'=>'failed to update location'
                 ]);
             }

         }else{
            $driver_location=new driver_location();
            $driver_location->driver_id=$r->driverId;
            $driver_location->driver_lat=$r->driver_lat;
            $driver_location->driver_long=$r->driver_long;
            $driver_location->save();
            return response()->json([
                'status'=>1,
                'message'=>'successfully save location'
            ]);
         }



    }

    public function AcceptBooking(Request $request)
    {
        $validatorUtils=new ValidationsUtil();
        $validatorUtils->setFields(['userId','driverId','bookingId']);
        if (!$validatorUtils->hasAllFields($request->all()))
        {
            return [
                'status' => 0,
                'message'=> $validatorUtils->getValidationErrorString()
            ];
        }
        $validator=Validator::make($request->all(),[

            'driverId'=>['required'],
            'bookingId'=>['required'],
             'userId'=>['required']
        ]);
        if ($validator->fails())
        {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }
        $conditions = [
            ['id','=',$request->driverId]
        ];
        $status=User::where($conditions)->first();
        if($status->account_verified)
        {

            $checkBookingStatus=bookingdetails::where('id','=',$request->bookingId)->first();
            if ($checkBookingStatus->status==1) {

                $bookingData = new bookingdetails();
                $resultData = $bookingData->get_accept_trip($request);
                if ($resultData) {

                    $title = "Accepted Booking";
                    $message = "Mr. " . $status['name'] . " and is been reaching to your location soon.";

                    try {
                        $token = FirebaseToken::where('customer_id', $request->userId)->firstOrFail();
                        $notification = new Notification();
                        $result = $notification->setTitle($title)->setMessage($message)->setToken($token->token)->transmitNotification();

                    } catch (ModelNotFoundException $exception) {
                    }

                    return response()->json([
                        'status' => 1,
                        'message' => 'Your booking is accepted'
                    ]);

                } else {

                    return response()->json([
                        'status' => 0,
                        'message' => 'oops! try again'
                    ]);
                }
            }else {
                return response()->json([
                    'status' => 0,
                    'message' => 'some one already accepted'
                ]);
            }

        }else
        {
            return response()->json([

                'status'=>0,
                'message'=>'Your account has been temporarily locked. Please contact our admin for further details.'

                ]);
        }

    }

    public function DriverRejectBooking(Request $request)
    {

        $validatorUtils=new ValidationsUtil();
        $validatorUtils->setFields(['userId','driverId','bookingId']);
        if (!$validatorUtils->hasAllFields($request->all()))
        {
            return [
                'status' => 0,
                'message'=> $validatorUtils->getValidationErrorString()
            ];
        }
        $validator=Validator::make($request->all(),[

            'driverId'=>['required'],
            'bookingId'=>['required'],
            'userId'=>['required']
        ]);
        if ($validator->fails())
        {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }
        $conditions = [
            ['id','=',$request->driverId]
        ];
        $status=User::where($conditions)->first();
        if($status->account_verified)
        {
            $checkBookingStatus=bookingdetails::where('id','=',$request->bookingId)->first();
            if ($checkBookingStatus->status==1) {
                $bookingData = new bookingdetails();
                $resultData = $bookingData->get_reject_trip($request);
                if ($resultData) {

                    $updateBookingData = bookingdetails::where('id', '=', $request->bookingId)->first();
                    $driverCount = $this->foundDriverList($updateBookingData);
                    if (count($driverCount) > 0) {
                        $sorted = collect($driverCount)->sortBy('distance');
                        $filterItem = $sorted->where("driver_id", $request->driverId)->delete();
                        $min = $sorted->where('distance', $filterItem->min('distance'))->first();
                        if ($min) {
                            $ct = driver_status::where('booking_id', '=', $request->bookingId)->get()->all();
                            if (count($ct) < 5) {
                                $DavailData = Driver_availability::where('driverId', '=', $min['driver_id'])->first();
                                if ($DavailData) {
                                    Driver_availability::where('driverId', '=', $min['driver_id'])->update([
                                        'driver_flag' => 2
                                    ]);
                                } else {
                                    $driverData = new Driver_availability();
                                    $driverData->driverId = $min['driver_id'];
                                    $driverData->driver_flag = 2;
                                    $driverData->save();
                                }
                                $data = new driver_status();
                                $data->driver_id = $min['driver_id'];
                                $data->booking_id = $request->bookingId;
                                $data->driver_flag = 2;
                                $data->save();
                                $title = "Incomming Booking";
                                $message = "You have received new Booking";
                                try {
                                    $msg = array
                                    (
                                        "bookingId" => $request->bookingId,
                                        "userId" => $request->userId
                                    );
                                    $token = FirebaseToken::where('customer_id', $min['driver_id'])->firstOrFail();
                                    $notification = new Notification();
                                    $resultN = $notification->setTitle($title)->setMessage($message)->setUserData($msg)->setToken($token->token)->transmit();

                                } catch (ModelNotFoundException $exception) {
                                }

                            } else {

                                $title = "Booking";
                                $message = "Oops! all our drivers are busy handling other client’s. We regret for inconvenience caused.";
                                try {
                                    $msg = array
                                    (
                                        "bookingId" => $request->bookingId,
                                        "userId" => $request->userId,
                                        "type" => 2,
                                    );
                                    $token = FirebaseToken::where('customer_id', $request->userId)->firstOrFail();
                                    $notification = new Notification();
                                    $resultN = $notification->setTitle($title)->setMessage($message)->setUserData($msg)->setToken($token->token)->transmit();

                                } catch (ModelNotFoundException $exception) {
                                }

                            }
                        }
                    }
                    return response()->json([
                        'status' => 1,
                        'message' => 'Your booking is Rejected'
                    ]);
                } else {

                    return response()->json([
                        'status' => 0,
                        'message' => 'some error accoured'
                    ]);
                }

            }else {

                return response()->json([
                    'status' => 0,
                    'message' => 'some one already accepted'
                ]);
            }

        }else
        {

            return response()->json([

                'status'=>0,
                'message'=>'Your account has been temporarily locked. Please contact our admin for further details.'

            ]);
        }



    }


    public function foundDriverList($Data)
    {
        $results = [];
        $conditions = [
            ['driver_lat', '!=', 0],
            ['driver_long', '!=', 0]
        ];
        $Coordinates = driver_location::where($conditions)->get();
        foreach ($Coordinates as $Coordinate)
        {
            $Latitude = $Coordinate->driver_lat;
            $Longitude = $Coordinate->driver_long;
            $options = [
                'sourceLat' => $Data->pickup_lat,
                'sourceLong' => $Data->pickup_long,
                'destinationLat' => $Latitude,
                'destinationLong' => $Longitude
            ];
            $distance = $this->distanceBetweenCoordinates($options);
            $checkStatus=User::where('id','=',$Coordinate->driver_id)->first();
            if ($distance <=self::MaxAllowedDistanceForNearestOutlet && $checkStatus->status==1){

                $results[] = [
                    'driver_id'=>$Coordinate->driver_id,
                    'distance'=>$distance,
                    'latitude' => $Coordinate->driver_lat,
                    'longitude' =>$Coordinate->driver_long
                ];

            }
        }
        if (count($results) > 0)
        {
            //check driver status is available or not
            $resultData = [];
            foreach ($results as $result)
            {
                $availability=Driver_availability::where('driverId','=',$result['driver_id'])->first();
                if ($availability)
                {
                    $availData=Driver_availability::where('driverId','=',$result['driver_id'])
                        ->where(function ($query){
                            $query->where('driver_flag','=',2)
                                ->orWhere('driver_flag','=',3);
                        })->first();
                    if ($availData){
                        $resultData[] = [
                            'driver_id'=>$result['driver_id'],
                            'distance'=>$result['distance'],
                            'latitude' => $result['latitude'],
                            'longitude' =>$result['latitude']
                        ];
                    }

                }else{
                    $resultData[] = [
                        'driver_id'=>$result['driver_id'],
                        'distance'=>$result['distance'],
                        'latitude' => $result['latitude'],
                        'longitude' =>$result['latitude']
                    ];
                }
            }
            if (count($resultData)>0) {
                return $resultData;
            }else {
                return $resultData;
            }

        }
        else {
            return $results;
        }

    }

    public function driver_arrived_trip(Request $request)
    {

        $validatorUtils=new ValidationsUtil();
        $validatorUtils->setFields(['userId','driverId','bookingId']);
        if (!$validatorUtils->hasAllFields($request->all()))
        {
            return [
                'status' => 0,
                'message'=> $validatorUtils->getValidationErrorString()
            ];
        }
        $validator=Validator::make($request->all(),[

            'driverId'=>['required'],
            'bookingId'=>['required'],
            'userId'=>['required']
        ]);
        if ($validator->fails())
        {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }
        $conditions = [
            ['id','=',$request->driverId],
        ];
        $status=User::where($conditions)->first();
        if($status->account_verified==1)
        {
            $bookingData=new bookingdetails();
            $status=$bookingData->get_arrived_trip($request);
            if ($status){

                $bookings=bookingdetails::where('id','=',$request->bookingId)->get()->first();
                if ($bookings)
                {
                    $userdetails=User::where('id','=',$request->userId)->first();
                    if ($userdetails)
                    {
                        $userData=[
                            'id'=>$userdetails->id,
                            'name'=>$userdetails->name,
                            'email'=>$userdetails->email,
                            'mobile'=>$userdetails->mobile,
                            'gender'=>$userdetails->gender,
                            'image'=>$userdetails->display_image
                        ];
                        $data= [
                            'bookingDetails'=>$bookings,
                            'userDetails'=>$userData
                        ];

                        $title = "Driver Arriving";
                        $message = "Mr.".$status['name']." has reached pickup point.";

                        try {
                            $token = FirebaseToken::where('customer_id', $request->userId)->firstOrFail();
                            $notification = new Notification();
                            $result = $notification->setTitle($title)->setMessage($message)->setToken($token->token)->transmitNotification();

                            if ($result){
                                return response()->json([
                                    'status' => 1,
                                    'data'=>$data,
                                    'message' =>'Update status successfully'
                                ]);
                            }else{

                                return response()->json([
                                    'status' => 0,
                                    'message' => 'Notification faild to send'
                                ]);
                            }


                        }
                        catch (ModelNotFoundException $exception) {

                            return response()->json([
                                'status' => 0,
                                'message' => $exception
                            ]);

                        }
                    }else{
                        return response()->json([
                            'status' => 0,
                            'message' => 'you have not booking yet'
                        ]);
                    }

                }else
                {
                    return response()->json([
                        'status' => 0,
                        'message' => 'no booking data found'
                    ]);
                }



            }else{

                return response()->json([
                    'status' => 0,
                    'message' => 'some error accoured'
                ]);
            }

        }else
        {

            return response()->json([

                'status'=>0,
                'message'=>'Your account has been temporarily locked. Please contact our admin for further details.'

            ]);
        }

    }

    public function driver_on_trip(Request $request)
    {

        $validatorUtils=new ValidationsUtil();
        $validatorUtils->setFields(['userId','driverId','bookingId']);
        if (!$validatorUtils->hasAllFields($request->all()))
        {
            return [
                'status' => 0,
                'message'=> $validatorUtils->getValidationErrorString()
            ];
        }
        $validator=Validator::make($request->all(),[

            'driverId'=>['required'],
            'bookingId'=>['required'],
            'userId'=>['required']
        ]);
        if ($validator->fails())
        {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }
        $conditions = [
            ['id','=',$request->driverId],
        ];
        $status=User::where($conditions)->first();
        if($status->account_verified==1)
        {
            $bookingData=new bookingdetails();
            $status=$bookingData->get_on_trip($request);
            if ($status){

                $bookings=bookingdetails::where('id','=',$request->bookingId)->get()->first();
                if ($bookings)
                {
                    $userdetails=User::where('id','=',$request->userId)->first();
                    if ($userdetails)
                    {
                        $userData=[
                            'id'=>$userdetails->id,
                            'name'=>$userdetails->name,
                            'email'=>$userdetails->email,
                            'mobile'=>$userdetails->mobile,
                            'gender'=>$userdetails->gender,
                            'image'=>$userdetails->display_image
                        ];
                        $data= [
                            'bookingDetails'=>$bookings,
                            'userDetails'=>$userData
                        ];
                        $title = "Start Trip";
                        $message = "Welcome ! Your Trip has began. We wish you safe journey.";

                        try {
                            $token = FirebaseToken::where('customer_id', $request->userId)->firstOrFail();
                            $notification = new Notification();
                            $result = $notification->setTitle($title)->setMessage($message)->setToken($token->token)->transmitNotification();

                            if ($result){
                                return response()->json([
                                    'status' => 1,
                                    'data'=>$data,
                                    'message' =>'Update status successfully'
                                ]);
                            }else{

                                return response()->json([
                                    'status' => 0,
                                    'message' => 'Notification faild to send'
                                ]);
                            }


                        }
                        catch (ModelNotFoundException $exception) {

                            return response()->json([
                                'status' => 0,
                                'message' => $exception
                            ]);

                        }
                    }else{
                        return response()->json([
                            'status' => 0,
                            'message' => 'you have not booking yet'
                        ]);
                    }

                }else
                {
                    return response()->json([
                        'status' => 0,
                        'message' => 'no booking data found'
                    ]);
                }


            }else{

                return response()->json([
                    'status' => 0,
                    'message' => 'some error  to update data'
                ]);
            }

        }else
        {

            return response()->json([

                'status'=>0,
                'message'=>'Your account has been temporarily locked. Please contact our admin for further details.'

            ]);
        }

    }


    public function driver_completed_trip(Request $request)
    {

        $validatorUtils=new ValidationsUtil();
        $validatorUtils->setFields(['userId','driverId','bookingId','final_amount']);
        if (!$validatorUtils->hasAllFields($request->all()))
        {
            return [
                'status' => 0,
                'message'=> $validatorUtils->getValidationErrorString()
            ];
        }
        $validator=Validator::make($request->all(),[

            'driverId'=>['required'],
            'bookingId'=>['required'],
            'userId'=>['required'],
            'final_amount'=>['required']
        ]);
        if ($validator->fails())
        {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }
        $conditions = [
            ['id','=',$request->driverId],
        ];
        $status=User::where($conditions)->first();
        if($status->account_verified==1)
        {
            $bookingData=new bookingdetails();
            $resultData=$bookingData->get_completed_trip($request);
            if ($resultData){

                $title = "Completed Trip";
                $message = "Welcome ! Your Trip has been completed";

                try {
                    $token = FirebaseToken::where('customer_id', $request->userId)->firstOrFail();
                    $notification = new Notification();
                    $result = $notification->setTitle($title)->setMessage($message)->setToken($token->token)->transmitNotification();

                }
                catch (ModelNotFoundException $exception) {
                }

                return response()->json([
                    'status' => 1,
                    'message' =>'Your booking is Completed'
                ]);

            }else{

                return response()->json([
                    'status' => 0,
                    'message' => 'some error  to update data'
                ]);
            }

        }else
        {

            return response()->json([

                'status'=>0,
                'message'=>'Your account has been temporarily locked. Please contact our admin for further details.'

            ]);
        }

    }




    public function distanceBetweenCoordinates(array $options){
        $sourceLat = $options['sourceLat'];
        $sourceLong = $options['sourceLong'];
        $destinationLat = $options['destinationLat'];
        $destinationLong = $options['destinationLong'];
        $theta = $sourceLong - $destinationLong;
        $dist = sin(deg2rad($sourceLat)) * sin(deg2rad($destinationLat)) + cos(deg2rad($sourceLat)) * cos(deg2rad($destinationLat)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $distance = $dist * 60 * 1.1515 * 1.609344;
        return intval($distance);
    }



}
