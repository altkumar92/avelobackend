<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\bookingdetails;
use App\Models\User;
use App\Utils\ValidationsUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookingController extends ApiController
{

    public function GetDriverBookingData(Request $request)
    {

        $validatorUtils=new ValidationsUtil();
        $validatorUtils->setFields(['customerId','bookingId']);
        if (!$validatorUtils->hasAllFields($request->all()))
        {
            return [
                'status' => 0,
                'message'=> $validatorUtils->getValidationErrorString()
            ];
        }
        $validator=Validator::make($request->all(),[

            'customerId'=>['required'],
            'bookingId'=>['required']
        ]);
        if ($validator->fails())
        {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }
        $bookings=null;
        $conditions = [
            ['id','=',$request->bookingId],
            ['user_id','=',$request->customerId]
        ];
        $bookings=bookingdetails::where($conditions)->get()->first();
        if ($bookings)
        {
            $userdetails=User::where('id','=',$bookings->user_id)->first();
            if ($userdetails)
            {
                $userData=[
                    'id'=>$userdetails->id,
                    'name'=>$userdetails->name,
                    'email'=>$userdetails->email,
                    'mobile'=>$userdetails->mobile,
                    'gender'=>$userdetails->gender,
                    'image'=>$userdetails->display_image
                ];

                $data= [
                    'bookingDetails'=>$bookings,
                    'userDetails'=>$userData
                ];

                return response()->json([
                    'status' => 1,
                    'message' => 'Your booking details ',
                    'data' =>$data
                ]);
            }else{
                return response()->json([
                    'status' => 0,
                    'message' => 'you have not booking yet'
                ]);
            }

        }else
        {
            return response()->json([
                'status' => 0,
                'message' => 'no booking data found'
            ]);
        }


    }

    public function GetUserBookingData(Request $request)
    {
        $validatorUtils=new ValidationsUtil();
        $validatorUtils->setFields(['bookingId']);
        if (!$validatorUtils->hasAllFields($request->all()))
        {
            return [
                'status' => 0,
                'message'=> $validatorUtils->getValidationErrorString()
            ];
        }
        $validator=Validator::make($request->all(),[

            'bookingId'=>['required']
        ]);
        if ($validator->fails())
        {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'fields' => $validator->errors()
            ];
        }
        $bookings=null;
        $conditions = [
            ['id','=',$request->bookingId]
        ];
        $bookings=bookingdetails::where($conditions)->get()->first();
        if ($bookings)
        {
            $userData=null;
            if ($bookings->assigned_for !=null) {
                $userdetails = User::where('id', '=', $bookings->assigned_for)->first();

                if ($userdetails) {
                    $userData = [
                        'id' => $userdetails->id,
                        'name' => $userdetails->name,
                        'email' => $userdetails->email,
                        'mobile' => $userdetails->mobile,
                        'gender' => $userdetails->gender,
                        'image' => $userdetails->display_image,
                        'vehicleColor'=>$userdetails->vehicle_color,
                        'registrationNo'=>$userdetails->vehicle_registration_number,
                    ];
                }
            }
            $data= [
                    'bookingDetails'=>$bookings,
                    'userDetails'=>$userData
              ];

            return response()->json([
                    'status' => 1,
                    'message' => 'Your booking details ',
                    'data' =>$data
            ]);


        }else
        {
            return response()->json([
                'status' => 0,
                'message' => 'Not a valid bookingId'
            ]);
        }




    }
}
