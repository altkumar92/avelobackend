<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\ApiController;
use App\Models\User;
use App\Utils\UploadsUtil;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/**
 * Handles uploading of various types of files across the entire app.
 * @package App\Http\Controllers\App
 */
class UploadsController extends ApiController{
    private $uploadsUtil = null;

    /**
     * Image headers, given here for mime recognition.
     */
    const ImageHeaders = [
        'Content-Type' => 'image/jpeg'
    ];

    /**
     * UploadsController constructor.
     */
    public function __construct(){
        $this->uploadsUtil = new UploadsUtil();
    }

    /**
     * Provides functionality to upload user display images for entire category of users.
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadAvatar(Request $request){
        $result = $this->uploadsUtil->uploadAvatar($request->file('image'));
        if ($result != null) {
            $response = [
                'status' => 1,
                'message' => 'Display image uploaded successfully.',
                'path' => $result
            ];
        }
        else {
            $response = [
                'status' => 0,
                'message' => 'We could not upload the image. Please try again later.',
                'path' => null
            ];
        }
        return response()->json($response);
    }

    /**
     * Allows download of an avatar image of an entity.
     * @param Request $request
     * @return mixed|null
     */
    public function downloadAvatar(Request $request){
        $userId = $request->userId;
        try {
            $user = User::findOrFail($userId);
            return Storage::download($user->display_image, 'user.jpeg', self::ImageHeaders);
        }
        catch (ModelNotFoundException $e) {
            return null;
        }
    }

    /**
     * Allows uploading of a driver's documents.
     * @param Request $r
     */
    public function uploadDriverDocuments(Request $r){
        $docs = [
            'rcFront' => $r->file('rcFront'),
            'rcBack' => $r->file('rcBack'),
            'dlFront' => $r->file('dlFront'),
            'dlBack' => $r->file('dlBack'),
        ];
        $response = $this->uploadsUtil->uploadDriverDocuments($docs);
        if ($response != null) {
            return response()->json([
                'status' => 1,
                'message' => 'All document images have been uploaded successfully.',
                'paths' => $response
            ]);
        }
        else {
            return response()->json([
                'status' => 0,
                'message' => 'One or more document images failed to upload. Please try again.',
                'paths' => null
            ]);
        }
    }
}
