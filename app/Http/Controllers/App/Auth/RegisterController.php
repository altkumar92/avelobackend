<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\ApiController;
use App\Models\OtpMapping;
use App\Models\PersonalAccessClient;
use App\Models\User;
use App\Utils\OtpUtils;
use App\Utils\ValidationsUtil;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Wraps API based registration functionality.
 * @package App\Http\Controllers\API\Auth
 */
class RegisterController extends ApiController{
    use RegistersUsers;

    /**
     * Returns user model if a user is found with that mobile number, false otherwise.
     * @param string $mobile
     * @return bool|User
     */
    public function findUserByMobile(string $mobile){
        return User::where('mobile', $mobile)->where('type', User::Roles['User'])->first();
    }

    /**
     * Returns user model if a user is found with that mobile number, false otherwise.
     * @param string $mobile
     * @return bool|User
     */
    public function findDriverByMobile(string $mobile){
        return User::where('mobile', $mobile)->where('type', User::Roles['Driver'])->first();
    }

    /**
     * Initiates registration procedure for a user.
     * At this point, only an OTP is sent to mobile number in context.
     * @param Request $r
     * @return array
     */
    public function userInitiateRegistration(Request $r){
        $validationsUtil = new ValidationsUtil();
        $validationsUtil->setFields(['mobile']);
        if (!$validationsUtil->hasAllFields($r->all())) {
            return [
                'status' => 0,
                'message' => $validationsUtil->getValidationErrorString(),
                'found' => false
            ];
        }

        $validator = Validator::make($r->all(), ['mobile' => ['required', 'string', 'size:10']]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => 'Your provided mobile number is not in valid format.',
                'found' => false
            ];
        }

        $user = null;
        if (!($user = $this->findUserByMobile($r->mobile))) {
            $otp = OtpUtils::generate();
            $mapping = OtpMapping::firstOrCreate(['mobile' => $r->mobile]);
            $mapping->mobile = $r->mobile;
            $mapping->otp = $otp;
            $mapping->save();
            $result = OtpUtils::sendAppOtp($otp, $r->mobile);
            if (!$result) {
                return [
                    'status' => 0,
                    'message' => 'We encountered an error sending otp to that mobile number. Please try again in a while.',
                    'found' => false
                ];
            }
            return [
                'status' => 1,
                'message' => 'We have sent an otp at your mobile number.',
                'found' => false
            ];
        }
        else {
            return [
                'status' => 0,
                'message' => 'Your provided mobile number is already taken. Try logging in or use a different one.',
                'found' => true
            ];
        }
    }

    /**
     * Completes registration for a user.
     * Takes all his details as input.
     * @param Request $r
     * @return array
     */
    public function userCompleteRegistration(Request $r){
        $validationsUtil = new ValidationsUtil();
        $validationsUtil->setFields(['mobile', 'otp', 'name', 'email','dob', 'password', 'gender', 'displayImage']);
        if (!$validationsUtil->hasAllFields($r->all())) {
            return [
                'status' => 0,
                'message' => $validationsUtil->getValidationErrorString(),
                'success' => false,
                'data' => null
            ];
        }

        $validator = Validator::make($r->all(), [
            'otp' => OtpUtils::getOTPValidationRule(),
            'mobile' => ['bail', 'required', 'digits:10'],
            'name' => ['bail', 'required', 'string', 'min:2', 'max:50'],
            'email' => ['bail', 'email', 'unique:users'],
            'password' => ['bail', 'required', 'string', 'min:4', 'max:64'],
            'gender' => ['bail', 'required', 'numeric', 'min:1', 'max:3'],
            'displayImage' => ['bail', 'required', 'string', 'min:1', 'max:255'],
             'dob' => ['bail', 'required']
        ]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'success' => false,
                'data' => null,
                'fields' => $validator->errors()
            ];
        }

        //TODO Add otp validation rule here, make sure to add a where condition to pick entry with request's OTP.
        //$entry = OtpMapping::where('mobile', $r->mobile)->where('otp', $r->otp)->first();
        $entry = OtpMapping::where('mobile', $r->mobile)->first();
        if (!$entry) {
            return [
                'status' => 0,
                'message' => 'Your provided otp does not match with the one sent to your mobile number.',
                'success' => false,
                'data' => null,
            ];
        }

        $user = null;
        $user = User::where('mobile', $r->mobile)->first();
        if ($user) {
            return [
                'status' => 0,
                'message' => 'Your provided mobile number is already taken. Try logging in or use a different one.',
                'success' => false,
                'data' => null,
            ];
        }
        else {
            $user = new User();
            $user->name = $r->name;
            $user->email = $r->email;
            $user->password = Hash::make($r->password);
            $user->type = User::Roles['User'];
            $user->mobile = $r->mobile;
            $user->gender = $r->gender;
            $user->dob = $r->dob;
            $user->display_image = $r->displayImage;
            $user->save();
            PersonalAccessClient::makeNewConditionally();
            $token = $user->createToken('Avelo')->accessToken;
            $data = [
                'id' => $user->id,
                'mobile' => $user->mobile,
                'name' => $user->name,
                'email' => $user->email,
                'token' => $token,
            ];
            return [
                'status' => 1,
                'message' => "You've been signed up successfully.",
                'success' => true,
                'data' => $data
            ];
        }
    }

    /**
     * Initiates registration procedure for a driver.
     * At this point, only an OTP is sent to mobile number in context.
     * @param Request $r
     * @return array
     */
    public function driverInitiateRegistration(Request $r){
        $validationsUtil = new ValidationsUtil();
        $validationsUtil->setFields(['mobile']);
        if (!$validationsUtil->hasAllFields($r->all())) {
            return [
                'status' => 0,
                'message' => $validationsUtil->getValidationErrorString(),
                'found' => false
            ];
        }

        $validator = Validator::make($r->all(), ['mobile' => ['required', 'string', 'size:10']]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => 'Your provided mobile number is not in valid format.',
                'found' => false
            ];
        }

        $user = null;
        if (!($user = $this->findDriverByMobile($r->mobile))) {
            $otp = OtpUtils::generate();
            $mapping = OtpMapping::firstOrCreate(['mobile' => $r->mobile]);
            $mapping->mobile = $r->mobile;
            $mapping->otp = $otp;
            $mapping->save();
            $result = OtpUtils::sendAppOtp($otp, $r->mobile);
            if (!$result) {
                return [
                    'status' => 0,
                    'message' => 'We encountered an error sending otp to that mobile number. Please try again in a while.',
                    'found' => false
                ];
            }
            return [
                'status' => 1,
                'message' => 'We have sent an otp at your mobile number.',
                'found' => false
            ];
        }
        else {
            return [
                'status' => 0,
                'message' => 'Your provided mobile number is already taken. Try logging in or use a different one.',
                'found' => true
            ];
        }
    }

    /**
     * Completes registration for a driver.
     * Takes all his details as input.
     * @param Request $r
     * @return array
     */
    public function driverCompleteRegistration(Request $r){
        $validationsUtil = new ValidationsUtil();
        $validationsUtil->setFields(['mobile', 'otp', 'name', 'email', 'password', 'gender', 'dob','displayImage', 'documents', 'dl_number', 'rc_number', 'vehicle_color', 'vehicle_registration_number', 'operational_city', 'rc_expiration_date']);
        if (!$validationsUtil->hasAllFields($r->all())) {
            return [
                'status' => 0,
                'message' => $validationsUtil->getValidationErrorString(),
                'success' => false,
                'data' => null
            ];
        }

        $validator = Validator::make($r->all(), [
            'otp' => OtpUtils::getOTPValidationRule(),
            'mobile' => ['bail', 'required', 'digits:10'],
            'name' => ['bail', 'required', 'string', 'min:2', 'max:50'],
            'email' => ['bail', 'email', 'unique:users'],
            'password' => ['bail', 'required', 'string', 'min:4', 'max:64'],
            'gender' => ['bail', 'required', 'numeric', 'min:1', 'max:3'],
            'displayImage' => ['bail', 'required', 'string', 'min:1', 'max:255'],
            'dob'=>['bail', 'required']
        ]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'success' => false,
                'data' => null,
                'fields' => $validator->errors()
            ];
        }

        //TODO Add otp validation rule here, make sure to add a where condition to pick entry with request's OTP.
        $entry = OtpMapping::where('mobile', $r->mobile)->first();
        if (!$entry) {
            return [
                'status' => 0,
                'message' => 'Your provided otp does not match with the one sent to your mobile number.',
                'success' => false,
                'data' => null,
            ];
        }

        $user = null;
        $user = User::where('mobile', $r->mobile)->where('type', User::Roles['Driver'])->first();
        if ($user) {
            return [
                'status' => 0,
                'message' => 'Your provided mobile number is already taken. Try logging in or use a different one.',
                'success' => false,
                'data' => null,
            ];
        }
        else {
            $documents = $r->documents;
            $user = new User();
            $user->name = $r->name;
            $user->email = $r->email;
            $user->password = Hash::make($r->password);
            $user->type = User::Roles['Driver'];
            $user->mobile = $r->mobile;
            $user->gender = $r->gender;
            $user->dob = $r->dob;
            $user->display_image = $r->displayImage;
            $user->dl_number = $r->dl_number;
            $user->dl_front_image = $documents['dlFront'];
            $user->dl_back_image = $documents['dlBack'];
            $user->rc_number = $r->rc_number;
            $user->rc_expiration_date = $r->rc_expiration_date;
            $user->rc_front_image = $documents['rcFront'];
            $user->rc_back_image = $documents['rcBack'];
            $user->vehicle_color = $r->vehicle_color;
            $user->vehicle_registration_number = $r->vehicle_registration_number;
            $user->operational_city = $r->operational_city;
            $user->save();
            PersonalAccessClient::makeNewConditionally();
            $token = $user->createToken('Avelo')->accessToken;
            $data = [
                'id' => $user->id,
                'mobile' => $user->mobile,
                'name' => $user->name,
                'email' => $user->email,
                'displayImage' => $user->display_image,
                'token' => $token,
            ];
            return [
                'status' => 1,
                'message' => "You've been signed up successfully.",
                'success' => true,
                'data' => $data
            ];
        }
    }
}
