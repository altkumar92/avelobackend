<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\ApiController;
use App\Models\OtpMapping;
use App\Models\PersonalAccessClient;
use App\Models\User;
use App\Utils\OtpUtils;
use App\Utils\ValidationsUtil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Wraps API based login functionality.
 * @package App\Http\Controllers\API\Auth
 */
class LoginController extends ApiController{

    /**
     * Returns user model if a user is found with that mobile number, false otherwise.
     * @param string $mobile
     * @return bool|User
     */
    public function findUserByMobile(string $mobile){
        return User::where('mobile', $mobile)->where('type', User::Roles['User'])->first();
    }

    /**
     * Returns user model if a user is found with that mobile number, false otherwise.
     * @param string $mobile
     * @return bool|User
     */
    public function findDriverByMobile(string $mobile){
        return User::where('mobile', $mobile)->where('type', User::Roles['Driver'])->first();
    }

    /**
     * Checks whether a customer exists for a mobile number.
     * @param Request $r
     * @return array
     */
    public function userExists(Request $r){
        $validationUtil = new ValidationsUtil();
        $validationUtil->setFields(['mobile']);
        if (!$validationUtil->hasAllFields($r->all())) {
            return [
                'status' => 0,
                'message' => $validationUtil->getValidationErrorString(),
                'found' => false
            ];
        }

        $validator = Validator::make($r->all(), ['mobile' => ['required', 'string', 'size:10']]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => 'Your provided mobile number is not in valid format.',
                'found' => false
            ];
        }

        $user = null;
        if (!($user = $this->findUserByMobile($r->mobile))) {
            return [
                'status' => 1,
                'message' => 'We could not find the user associated with that mobile number.',
                'found' => false
            ];
        }
        else {
            $otp = OtpUtils::generate();
            $user->otp = $otp;
            $user->save();
            $result = OtpUtils::sendAppOtp($otp, $r->mobile);
            if (!$result) {
                return [
                    'status' => 0,
                    'message' => 'We encountered an error sending otp to that mobile number. Please try again in a while.',
                    'found' => false
                ];
            }
            return [
                'status' => 1,
                'message' => 'We have sent an otp at your registered mobile number.',
                'found' => true
            ];
        }
    }

    /**
     * Checks whether a customer exists for a mobile number.
     * @param Request $r
     * @return array
     */
    public function driverExists(Request $r){
        $validationUtil = new ValidationsUtil();
        $validationUtil->setFields(['mobile']);
        if (!$validationUtil->hasAllFields($r->all())) {
            return response()->json([
                'status' => 0,
                'message' => $validationUtil->getValidationErrorString(),
                'found' => false
            ]);
        }

        $validator = Validator::make($r->all(), ['mobile' => ['required', 'string', 'size:10']]);
        if ($validator->fails()) {
            return response()->json([
                'status' => 0,
                'message' => 'Your provided mobile number is not in valid format.',
                'found' => false
            ]);
        }

        $user = null;
        if (!($user = $this->findDriverByMobile($r->mobile))) {
            return response()->json([
                'status' => 1,
                'message' => 'We could not find the driver associated with that mobile number.',
                'found' => false
            ]);
        }
        else {
            $otp = OtpUtils::generate();
            $user->otp = $otp;
            $user->save();
            $result = OtpUtils::sendAppOtp($otp, $r->mobile);
            if (!$result) {
                return [
                    'status' => 0,
                    'message' => 'We encountered an error sending otp to that mobile number. Please try again in a while.',
                    'found' => false
                ];
            }
            return [
                'status' => 1,
                'message' => 'We have sent an otp at your registered mobile number.',
                'found' => true
            ];
        }
    }

    /**
     * Attempts to log in a user matching the OTP and and mobile
     * number given by him.
     * @param Request $r
     * @return array
     */
    public function userLogin(Request $r){
        $validationUtil = new ValidationsUtil();
        $validationUtil->setFields(['mobile', 'otp']);
        if (!$validationUtil->hasAllFields($r->all())) {
            return [
                'status' => 0,
                'message' => $validationUtil->getValidationErrorString(),
                'success' => false,
                'data' => null
            ];
        }

        $validator = Validator::make($r->all(), [
            'otp' => OtpUtils::getOTPValidationRule(),
            'mobile' => ['bail', 'required', 'digits:10'],
        ]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => 'One or more of your provided details failed validation. Please try again.',
                'success' => false,
                'data' => null,
                'fields' => $validator->errors()
            ];
        }

        $user = null;
        if (!($user = $this->findUserByMobile($r->mobile))) {
            return [
                'status' => 0,
                'message' => 'We could not find the user associated with that mobile number.',
                'success' => false,
                'data' => null
            ];
        }
        else {
            $user = $this->findUserByMobile($r->mobile);
            if (OtpMapping::matches($user, $r->otp)) {
                PersonalAccessClient::makeNewConditionally();
                $token = $user->createToken('Avelo')->accessToken;
                $data = [
                    'id' => $user->id,
                    'mobile' => $r->mobile,
                    'name' => $user->name,
                    'dob' => $user->dob,
                    'email' => $user->email,
                    'token' => $token,
                ];
                return [
                    'status' => 1,
                    'message' => "You've been logged in successfully.",
                    'success' => true,
                    'data' => $data
                ];
            }
            else {
                return [
                    'status' => 0,
                    'message' => 'The otp you typed does not match with the one we sent on your registered mobile.',
                    'success' => false,
                    'data' => null
                ];
            }
        }
    }

    /**
     * Attempts to log in a driver after Otp verification.
     * @param Request $r
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function driverLogin(Request $r){
        $validationUtil = new ValidationsUtil();
        $validationUtil->setFields(['mobile', 'password']);
        if (!$validationUtil->hasAllFields($r->all())) {
            return [
                'status' => 0,
                'message' => $validationUtil->getValidationErrorString(),
                'success' => false,
                'data' => null
            ];
        }

        $validator = Validator::make($r->all(), [
            'mobile' => ['bail', 'required', 'digits:10'],
            'password' => ['bail', 'required', 'string', 'min:8', 'max:32'],
        ]);
        if ($validator->fails()) {
            return [
                'status' => 0,
                'message' => $validator->errors()->first(),
                'success' => false,
                'data' => null,
                'fields' => $validator->errors()
            ];
        }

        $user = null;
        if (!($user = $this->findDriverByMobile($r->mobile))) {
            return [
                'status' => 1,
                'message' => 'We could not find the driver associated with that mobile number.',
                'success' => false,
                'data' => null
            ];
        }
        else {
            $user = $this->findDriverByMobile($r->mobile);
            $password = $r->password;
            if ($user != false && $user->account_verified == false) {
                return response()->json([
                    'status' => 0,
                    'message' => 'Your account is not yet verified. Please contact your administrator.',
                    'success' => true,
                    'data' => null
                ]);
            }
            else if ($user != false && Hash::check($password, $user->password)) {
                PersonalAccessClient::makeNewConditionally();
                $token = $user->createToken('Avelo')->accessToken;
                $data = [
                    'id' => $user->id,
                    'mobile' => $r->mobile,
                    'name' => $user->name,
                    'dob' => $user->dob,
                    'email' => $user->email,
                    'displayImage' => $user->display_image,
                    'token' => $token

                ];
                return response()->json([
                    'status' => 1,
                    'message' => "You've been logged in successfully.",
                    'success' => true,
                    'data' => $data
                ]);
            }
            else {
                return response()->json([
                    'status' => 0,
                    'message' => 'You have entered an invalid password for your account. Please try again.',
                    'success' => false,
                    'data' => null
                ]);
            }
        }
    }
}
