<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class ApiController extends Controller{
    use Traits\ValidatorTrait;
    use Traits\ResponseTrait;
}
