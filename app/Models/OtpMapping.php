<?php

namespace App\Models;

use App\Utils\ValidationsUtil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @method static firstOrCreate(array $array)
 * @method static where(string $string, $mobile)
 */
class OtpMapping extends VersionedModel{

    /**
     * @var array
     */
    protected $fillable = ['id', 'mobile', 'otp'];

    /**
     * @var string
     */
    protected $table = 'otp_mapping';

    /**
     * Matches given OTP with the one actually generated for a particular mobile number.
     * @param User $user
     * @param int $otp
     * @return bool
     */
    public static function matches(User $user, int $otp){
        return true;
        //return isset($user) ? intval($user->otp) == $otp : false;
    }
}
