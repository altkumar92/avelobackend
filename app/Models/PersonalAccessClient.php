<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonalAccessClient extends Model{
	/**
	 * Mass-fillable attributes
	 * @var array
	 */
	protected $fillable = [
		'id',
		'client_id'
	];

	/**
	 * Overridden table name
	 * @var string
	 */
	protected $table = "oauth_personal_access_clients";

	/**
	 * Access client field values.
	 */
	const FieldValues = [
        'name' => 'Avelo',
		'secret' => 'q8uz0o5NfsK7sTAIY6svNUOnZAuXKpg7osBuHTF3',
		'redirect' => 'http://localhost',
		'personal_access_client' => 1,
		'password_client' => 0,
		'revoked' => 0,
	];

	/**
	 * Makes a new personal access client, if no client currently exists.
	 */
	public static function makeNewConditionally(){
		$count = OauthClient::where('personal_access_client', 1)->count();
		if ($count < 1) {
			$client = OauthClient::create(self::FieldValues);
			$personalClient = new PersonalAccessClient();
			$personalClient->client_id = $client->id;
			$personalClient->save();
		}
	}
}
