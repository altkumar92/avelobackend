<?php

namespace App\Models;

use App\Utils\GUID;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * @method static where(string $string, string $value)
 */
class UserSession extends VersionedModel{
	/**
	 * @var array
	 */
	protected $fillable = ['id', 'customer_id', 'session_id'];

	/**
	 * @var string
	 */
	protected $table = "user_sessions";

	/**
	 * Start a new session for an anonymous user.
	 * @return array
	 */
	public static function makeNew(){
		$guid = GUID::make();
		$userSession = new UserSession();
		$userSession->session_id = $guid;
		$userSession->save();
		return $guid;
	}
}