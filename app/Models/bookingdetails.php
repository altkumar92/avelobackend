<?php

namespace App\Models;

use App\Models\driver_status;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;


class bookingdetails extends Model
{

    public function CreateBookingId($request)
    {


        $data=new bookingdetails();
         $data->username=$request->username;
        $data->user_id=$request->user_id;
        $data->pickup_area=$request->pickup_area;
        $data->drop_area=$request->drop_area;
        $data->distance=$request->distance;
        $data->approx_time=$request->approx_time;
        $data->amount=$request->amount;
        $data->final_amount=$request->final_amount;
        $data->payment_type=$request->payment_type;
        $data->pickup_lat=$request->pickup_lat;
        $data->pickup_long=$request->pickup_long;
        $data->drop_lat=$request->drop_lat;
        $data->drop_long=$request->drop_long;
        $data->flag=0;
        $data->status=1;
        $data->status_code="pending";
        try {
            $data->save();
            return $data;
        }
        catch (\Exception $e) {
            return false;
        }

    }

    function get_accept_trip($request)
    {


        if($request->bookingId!='' && $request->driverId!=''
            && $request->userId!='')
        {
            $conditions = [
                ['id','=',$request->bookingId],
                ['user_id','=',$request->userId],
            ];
            $condStatus = [
                ['booking_id','=',$request->bookingId],
                ['driver_id','=',$request->driverId],
            ];

            try {
                $data=bookingdetails::where($conditions)->update([
                    'status' => '3',
                    'status_code' => 'accepted',
                    'assigned_for' => $request->driverId
                ]);
                driver_status::where($condStatus)->update([
                    'driver_flag' => '1',
                ]);
                Driver_availability::where("driverId",'=',$request->driverId)->update([
                    'driver_flag' => '1',
                ]);

                return $data;

            }catch (\Exception $ex){
               return false;
            }

        }
    }

    function get_reject_trip($request)
    {


        if($request->bookingId!='' && $request->driverId!=''
            && $request->userId!='')
        {
            $conditions = [
                ['id','=',$request->bookingId],
                ['user_id','=',$request->userId],
            ];
            $condStatus = [
                ['booking_id','=',$request->bookingId],
                ['driver_id','=',$request->driverId],
            ];

            try {
                $data=bookingdetails::where($conditions)->update([
                    'status' => '5',
                    'status_code' => 'driver-cancelled'
                ]);
                driver_status::where($condStatus)->update([
                    'driver_flag' => '2',
                ]);
                Driver_availability::where("driverId",'=',$request->driverId)->update([
                    'driver_flag' => '2',
                ]);

                return $data;

            }catch (\Exception $ex){
                return false;
            }

        }

    }


    function get_arrived_trip($request)
    {


        if($request->bookingId!='' && $request->driverId!=''
            && $request->userId!='')
        {
            $conditions = [
                ['id','=',$request->bookingId],
                ['user_id','=',$request->userId],
            ];
            try {
                $data=bookingdetails::where($conditions)->update([
                    'status' => '7',
                    'status_code' => 'driver-arrived'
                ]);
                return $data;

            }catch (\Exception $ex){
                return false;
            }

        }

    }



    function get_on_trip($request)
    {


        if($request->bookingId!='' && $request->driverId!=''
            && $request->userId!='')
        {
            $conditions = [
                ['id','=',$request->bookingId],
                ['user_id','=',$request->userId],
            ];
            try {
                $data=bookingdetails::where($conditions)->update([
                    'status' => '8',
                    'status_code' => 'on-trip'
                ]);
                return $data;

            }catch (\Exception $ex){
                return false;
            }

        }

    }

    function get_completed_trip($request)
    {


        if($request->bookingId!='' && $request->driverId!=''
            && $request->userId!='')
        {
            $conditions = [
                ['id','=',$request->bookingId],
                ['user_id','=',$request->userId],
            ];

            $condStatus = [
                ['booking_id','=',$request->bookingId],
                ['driver_id','=',$request->driverId],
            ];
            try {
                $data=bookingdetails::where($conditions)->update([
                    'status' => '9',
                    'status_code' => 'completed',
                    'final_amount' => $request->final_amount,
                    'reason' => 'delay'
                ]);
                driver_status::where($condStatus)->update([
                    'driver_flag' => '3'
                ]);
                Driver_availability::where("driverId")->update([
                    'driver_flag' => '3',
                ]);
                return $data;
            }catch (\Exception $ex){
                return false;
            }

        }

    }



    public function user_reject_trip($request)
    {

        if($request->bookingId!='')
        {
            $conditions = [
                ['id','=',$request->bookingId]
            ];

            try {
                $data=bookingdetails::where($conditions)->update([
                    'status' => '4',
                    'status_code' => 'user-cancelled'
                ]);
                Driver_availability::where("driverId",'=',$request->driverId)->update([
                    'driver_flag' => '2',
                ]);
                return $data;

            }catch (\Exception $ex){
                return false;
            }

        }

    }

    function updateimmidiatedriverstatus($booking_id)
    {
        $result=driver_status::where('booking_id','=', $booking_id)->get()->all();
        if($result)
        {
            foreach($result as $row)
            {

                driver_status::where('booking_id','=',$booking_id)
                    ->where('driver_id','=',$row['driver_id'])->update([
                        'driver_flag' => 2
                    ]);

            }
        }
    }



}
