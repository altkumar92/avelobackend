<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Driver_availability extends Model
{
    protected $table = 'driver_availabilities';
    protected $fillable = ['driverId', 'driver_flag'];
}
