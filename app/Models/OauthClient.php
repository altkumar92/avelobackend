<?php

namespace App\Models;

class OauthClient extends VersionedModel{

    protected $fillable = [
        'id',
        'user_id',
        'name',
        'secret',
        'redirect',
        'password_client',
        'personal_access_client',
        'revoked'
    ];

    protected $table = "oauth_clients";
}
