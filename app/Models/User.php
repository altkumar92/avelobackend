<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

/**
 * @method static where(string $string, string $mobile)
 */
class User extends Authenticatable{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile', 'gender', 'type', 'otp', 'firebase_token', 'display_image', 'dl_number', 'dl_front_image', 'dl_back_image', 'rc_number', 'rc_front_image', 'rc_back_image', 'vehicle_color', 'vehicle_registration_number', 'operational_city'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Defines what kind of a user if represented by what number internally.
     */
    const Roles = [
        'Admin' => 1,
        'Driver' => 2,
        'User' => 3
    ];
}
