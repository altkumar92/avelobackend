<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Our own custom variant of base model which supports calling versioned methods.
 * @package App\Models
 */
class VersionedAuthenticatable extends Authenticatable{

	/**
	 * @var bool
	 */
	protected static $shouldFallbackToFirstVersion = false;

	/**
	 * Sets whether the callee should override versioning and call the first version of all versioned methods.
	 * @param bool $fallback
	 */
	public static function setShouldFallbackToFirstVersion(bool $fallback){
		VersionedAuthenticatable::$shouldFallbackToFirstVersion = $fallback;
	}

	/**
	 * Calls a method taking into account its version.
	 * @param string $methodName Name of method to be called, must exist in the class in context.
	 * @param $parameter mixed Parameter to be passed to called function. Generally this will be the Request object instance.
	 * @param int $version Version of the method to call, if no version is specified, version 1 is considered as default.
	 * @return
	 */
	public function callMethodVersion(string $methodName, $parameter, int $version = 1){
		if (VersionedAuthenticatable::$shouldFallbackToFirstVersion) {
			$version = 1;
		}

		$methodRef = sprintf("%sV%d", $methodName, $version);
		if (method_exists($this, $methodRef)) {
			return $this->$methodRef($parameter);
		}
		return ['status' => 0, 'message' => 'Method does not exist in called class.'];
	}
}