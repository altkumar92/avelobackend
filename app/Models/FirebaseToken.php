<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utils\ValidationsUtil;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class FirebaseToken extends Model
{
    /**
     * @var string
     */
    protected $table = "firebase_tokens";

    /**
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'device_id',
        'token'
    ];

    /**
     * Updates Firebase token for a customer or device.
     * @param Request $request
     * @return JsonResponse
     */
    public function updateToken(Request $request){
        $validationsUtil = new ValidationsUtil();
        $validationsUtil->setFields(['customerId', 'deviceId', 'token']);
        if (!$validationsUtil->hasAllFields($request->all())) {
            return response()->json([
                'status' => 0,
                'message' => $validationsUtil->getValidationErrorString()
            ]);
        }

        $customerId = $request->customerId;
        $deviceId = $request->deviceId;
        $token = $request->token;

        try {
            $tokenRef = FirebaseToken::where('customer_id', $customerId)->firstOrFail();
            $tokenRef->token = $token;
            $tokenRef->device_id = $deviceId;
            $tokenRef->save();
        }
        catch (ModelNotFoundException $e) {
            $tokenRef = new FirebaseToken();
            $tokenRef->token = $token;
            $tokenRef->device_id = $deviceId;
            $tokenRef->customer_id = $customerId;
            $tokenRef->save();
        }
        return response()->json([
            'status' => 1,
            'message' => 'Firebase token was updated successfully.'
        ]);
    }
}
